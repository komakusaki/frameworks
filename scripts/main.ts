import { ModelService } from './services/modelService/ModelService';
import { EventBus } from './services/eventbus/EventBus';
import { IMetadata } from './interfaces/IMetadata';
import { Elements } from './components/Elements';
import * as template from './template.html!text';
import { Utils } from './utils/Utils';
import { DOMParserService } from './services/DOMParserService/DOMParserService';

export class App {
    modelServiceInstance: ModelService;

    constructor(host: HTMLElement) {
        let inputData = {
            version: '1',
            personalData: {
                name: 'Krzysztof',
                surname: 'Lipiec'
            },
            objectArray: [
                {
                    name: 'Krzysztof',
                    surname: 'Lipiec'
                },
                {
                    name: 'Jan',
                    surname: 'Kowalski'
                }
            ]
        };
        let jsonTemplate: IMetadata = {
            "context": "personalData",
            "element": "container-control",
            "children": [
                {
                    "element": "label-control",
                    "context": "name"
                }, {
                    "element": "label-control",
                    "context": "surname"
                },
                {
                    "element": "text-field",
                    "context": "name"
                }
            ]
        };
        let eventBus = new EventBus();
        this.modelServiceInstance = new ModelService(inputData);

        let model = this.modelServiceInstance.getModel();


        let DOMTemplate = Utils.createElementFromTemplate(template as unknown as string);
        DOMParserService.parseTemplate(DOMTemplate, model, eventBus, null);


        // let mainComponent = new Elements['container-control']({
        //     children: [jsonTemplate]
        // }, eventBus);
        // mainComponent.setData(model);
        // mainComponent.render(host);
        host.appendChild(DOMTemplate);
        model.data['objectArray'].push({ name: 'Paulina', surname: 'lipiec' });
        console.log(model.data);

        eventBus.on('modelChange', () => {
            console.log(inputData);
        });
    }
}