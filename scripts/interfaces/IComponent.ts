export interface IComponent {
    isBuilt: () => boolean;
    render: (host: HTMLElement) => void;
    setData: (model: unknown) => void;
    getData: () => unknown;
    updateValue: (value: unknown) => void;
}