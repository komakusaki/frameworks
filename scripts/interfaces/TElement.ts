export enum TElement {
    'container-control' = 'container',
    'text-field' = 'textField',
    'label-control' = 'label',
    'list-control' = 'list'
}