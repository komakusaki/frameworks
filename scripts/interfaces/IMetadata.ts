import { TElement } from './TElement';

export interface IMetadata {
    element?: keyof typeof TElement;
    context?: string;
    children?: Array<IMetadata> | Array<HTMLElement>;
    template?: HTMLElement;
}