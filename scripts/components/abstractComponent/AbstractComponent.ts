import { ModelService } from '../../services/modelService/ModelService';
import { EventBus } from '../../services/eventbus/EventBus';
import { IMetadata } from '../../interfaces/IMetadata';
import { IComponent } from '../../interfaces/IComponent';

export abstract class AbstractComponent implements IComponent {
    protected componentElement: HTMLElement;
    protected children: Array<AbstractComponent> = [];
    protected parent: AbstractComponent;
    protected model: unknown;

    constructor(protected metadata: IMetadata, protected eventBus: EventBus, protected path: string = 'data') {
    }

    protected abstract build(): void;

    protected abstract updateView(): void;

    public isBuilt(): boolean {
        return !!this.componentElement;
    }

    public addChild(child: AbstractComponent): void {
        this.children.push(child);
        child.setParent(this);
    }

    public setParent(parent: AbstractComponent): void {
        this.parent = parent;
    }

    public removeChildren(): void {
        this.children.forEach((child: AbstractComponent) => {
            child.removeChildren();
            this.eventBus.off('modelChange', this.onModelChange);
        })
    }

    public render(host: HTMLElement, before?: HTMLElement): void {
        if (!this.isBuilt()) {
            this.build();
        }
        if (before) {
            Array.from(before.attributes).forEach(({ name, value }: { name: string, value: string }) => {
                if (name === 'class') {
                    before.classList.forEach((className: string) => {
                        this.componentElement.classList.add(className);
                    })
                } else {
                    this.componentElement.setAttribute(name, value);
                }
            });
            host.insertBefore(this.componentElement, before);
        } else {
            host.appendChild(this.componentElement);
        }
        this.componentElement.parentNode
    }

    protected onModelChange(path: string): void {
        console.log('modelChange');
        if (this.isCurrentPath(path)) {
            this.updateView();
        }
    }

    public setData(model: unknown): void {
        this.model = model;
        ModelService.setProxy(this.path, model, this.eventBus);
        this.onModelChange = this.onModelChange.bind(this);
        this.eventBus.on('modelChange', this.onModelChange);
    }

    protected isCurrentPath(path: string): boolean {
        return path === this.path;
    }

    public getData(): unknown {
        return this.getContext()[this.getPropertyName()];
    }

    private getPropertyName(): string {
        let pathArray = this.path.split('.');
        return pathArray.pop();
    }

    private getContext(): unknown {
        let pathArray = this.path.split('.'),
            context = this.model || {};
        pathArray.pop();
        pathArray.forEach((pathPart: string) => {
            if (context[pathPart]) {
                context = context[pathPart];
            } else {
                context = {};
            }
        });
        return context;
    }

    public updateValue(value: unknown): void {
        this.getContext()[this.getPropertyName()] = value;
    }
}