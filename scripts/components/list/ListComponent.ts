import { AbstractComponent } from '../abstractComponent/AbstractComponent';
import { DOMParserService } from '../../services/DOMParserService/DOMParserService';

export class ListComponent extends AbstractComponent {
    private itemTemplate: HTMLElement;

    protected build(): void {
        this.componentElement = document.createElement('div');
        this.itemTemplate = (this.metadata.children as Array<HTMLElement>)[0];
        this.updateView();
    }

    protected isCurrentPath(path: string): boolean {
        let arrayPath = path.split('.');
        arrayPath.pop();
        return arrayPath.join('.') === this.path;
    }

    protected updateView(): void {
        let array = this.getData() as Array<unknown>;
        this.removeChildren();
        this.componentElement.innerHTML = '';
        array.forEach((data: unknown, index: number) => {
            let template = this.itemTemplate.cloneNode(true) as HTMLElement;
            DOMParserService.parseTemplate(
                template,
                this.model,
                this.eventBus,
                this,
                `${this.path}.${index}`
            );
            this.componentElement.appendChild(template);
        });
    }
}