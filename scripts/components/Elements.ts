import { Container } from './container/Container';
import { TextField } from './textField/TextField';
import { Label } from './label/Label';
import { IComponent } from '../interfaces/IComponent';
import { IMetadata } from '../interfaces/IMetadata';
import { EventBus } from '../services/eventbus/EventBus';
import { TElement } from '../interfaces/TElement';
import { ListComponent } from './list/ListComponent';

interface IComponentClass {
    new(
        metadata: IMetadata,
        eventBus: EventBus,
        path?: string
    ): IComponent;
}

export const Elements: { [key in keyof typeof TElement]: IComponentClass } = {
    'container-control': Container,
    'text-field': TextField,
    'label-control': Label,
    'list-control': ListComponent
};