import { AbstractComponent } from '../abstractComponent/AbstractComponent';
import * as template from './TextField.html!text';
import { Utils } from '../../utils/Utils';

export class TextField extends AbstractComponent {
    private inputElement: HTMLInputElement;

    protected build(): void {
        this.componentElement = Utils.createElementFromTemplate(template as unknown as string);
        this.inputElement = Utils.findFirstByAttribute(
            this.componentElement,
            'data-text-field-component-input'
        ) as HTMLInputElement;
        this.inputElement.addEventListener('keyup', () => {
            if (this.getData() !== this.inputElement.value) {
                this.updateValue(this.inputElement.value)
            }
        });
        this.updateView();
    }

    protected updateView(): void {
        this.inputElement.value = this.getData() as string || '';
    }
}