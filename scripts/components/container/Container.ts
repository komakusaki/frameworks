import { AbstractComponent } from '../abstractComponent/AbstractComponent';
import * as template from './Container.html!text';
import { Utils } from '../../utils/Utils';
import { IMetadata } from '../../interfaces/IMetadata';
import { Elements } from '../Elements';
import { DOMParserService } from '../../services/DOMParserService/DOMParserService';

export class Container extends AbstractComponent {
    protected build(): void {
        this.componentElement = Utils.createElementFromTemplate(template as unknown as string);
        if (this.metadata.children) {
            this.metadata.children.forEach((childMetadata: IMetadata | HTMLElement): void => {
                if (childMetadata instanceof HTMLElement) {
                    DOMParserService.parseTemplate(childMetadata, this.model, this.eventBus, this.path);
                } else {
                    let child = new Elements[childMetadata.element](
                        childMetadata,
                        this.eventBus,
                        this.path ? `${this.path}.${childMetadata.context}` : childMetadata.context
                    );
                    child.setData(this.model);
                    child.render(Utils.findFirstByAttribute(this.componentElement, 'data-container-content'));
                }
            });
        }
    }

    protected updateView(): void {
    }
}