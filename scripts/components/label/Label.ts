import { AbstractComponent } from '../abstractComponent/AbstractComponent';
import { Utils } from '../../utils/Utils';
import * as template from './Label.html!text';

export class Label extends AbstractComponent {
    protected build(): void {
        this.componentElement = Utils.createElementFromTemplate(template as unknown as string);
        this.updateView();
    }

    protected updateView(): void {
        Utils.findFirstByAttribute(this.componentElement, 'data-label-container').innerText = this.getData() as string;
    }
}