import { Elements } from '../../components/Elements';
import { AbstractComponent } from '../../components/abstractComponent/AbstractComponent';

export class DOMParserService {
    public static parseTemplate(
        template: HTMLElement,
        model: unknown,
        eventBus: unknown,
        parent: AbstractComponent,
        path: string = 'data'
    ): void {
        let context = template.getAttribute('context');
        if (context) {
            path = path ? `${path}.${context}` : context;
        }
        Array.from(template.children).forEach((child: HTMLElement) => {
            let context = child.getAttribute('context');
            if (Elements[child.tagName.toLowerCase()]) {
                let component = new Elements[child.tagName.toLocaleLowerCase()](
                    {
                        children: Array.from(child.children)
                    },
                    eventBus,
                    path ? `${path}.${context}` : context
                );
                component.setData(model);
                component.render(template, child);
                if (parent) {
                    parent.addChild(component);
                }
                child.remove();
            } else {
                this.parseTemplate(child, model, eventBus, parent, path);
            }
        });
    }
}